# frozen_string_literal: true

# BEGIN
def count_letters(word)
  letters = {}
  word.each_char do |c|
    letters[c] = letters.fetch(c, 0) + 1
  end
  letters
end

def anagramm_filter(word, anagramms)
  original_letters = count_letters(word)
  anagramms.filter { |w| count_letters(w) == original_letters }
end
# END
