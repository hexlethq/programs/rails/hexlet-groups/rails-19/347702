# frozen_string_literal: true

# BEGIN
def get_same_parity((first, *rest))
  return [] if first.nil?

  result = [first]
  result.concat rest.select(&:even?) if first.even?
  result.concat rest.select(&:odd?) if first.odd?
  result
end
# END
