# frozen_string_literal: true

# BEGIN
def build_query_string(options)
  options.sort_by { |k, _| k }
         .map { |item| "#{item[0]}=#{item[1]}" }
         .join('&')
end
# END
