# frozen_string_literal: true

# BEGIN
def compare_versions(version1, version2)
  return nil if version1.empty? || version2.empty?

  maj1, min1 = version1.split('.')
  maj2, min2 = version2.split('.')
  return min1.to_i <=> min2.to_i if maj1 == maj2

  maj1.to_i <=> maj2.to_i
end
# END
