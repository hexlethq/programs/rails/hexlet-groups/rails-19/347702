# frozen_string_literal: true

# BEGIN
def fibonacci(number)
  return nil if number <= 0

  actual_fibonacci(number - 1)
end

def actual_fibonacci(number)
  return 0 if number.zero?

  last_num = 0
  next_num = 1
  1.upto(number - 1) do
    last_num, next_num = next_num, last_num + next_num
  end
  next_num
end
# END
