# frozen_string_literal: true

def make_censored(text, stop_words)
  # BEGIN
  censored = '$#%!'
  modified_text = text.dup
  stop_words.each do |word|
    modified_text.gsub!(/(?<!\S)#{Regexp.quote(word)}(?!\S)/, censored)
  end
  modified_text

  # END
end
