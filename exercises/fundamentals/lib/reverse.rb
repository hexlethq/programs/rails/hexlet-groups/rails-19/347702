# frozen_string_literal: true

# BEGIN
def reverse(line)
  result = ''
  counter = line.length - 1
  while counter >= 0
    result += line[counter]
    counter -= 1
  end
  result
end
# END
