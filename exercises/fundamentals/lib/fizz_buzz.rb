# frozen_string_literal: true

# BEGIN
def fizz_buzz(start, stop)
  result = []
  start.upto(stop) do |i|
    temp = ''
    temp += 'Fizz' if (i % 3).zero?
    temp += 'Buzz' if (i % 5).zero?
    temp = i.to_s if temp.empty?
    result << temp
  end
  result.join(' ')
end
# END
